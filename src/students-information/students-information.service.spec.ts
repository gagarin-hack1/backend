import { Test, TestingModule } from '@nestjs/testing';
import { StudentsInformationService } from './students-information.service';

describe('StudentsInformationService', () => {
  let service: StudentsInformationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StudentsInformationService],
    }).compile();

    service = module.get<StudentsInformationService>(StudentsInformationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
