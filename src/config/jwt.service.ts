import { ConfigModule, ConfigService } from "@nestjs/config";
import { JwtModuleAsyncOptions, JwtModuleOptions } from '@nestjs/jwt';

const jwtConfig: JwtModuleAsyncOptions = {
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (configService: ConfigService): Promise<JwtModuleOptions> => {
        return {
            global: true,
            secret: configService.get('JWT_SECRET') as string,
            signOptions: { expiresIn: '1h' }
        };
    }
};

export default jwtConfig;
