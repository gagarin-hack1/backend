import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum TYPES_ENUM {
  'orientation'  = 'orientation',
  'socialization'  = 'socialization',
  'study'  = 'study',
  'career'  = 'career',
}

@Entity('student-information')
export class StudentInformation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int' })
  studentId: number;

  @Column({ type: 'enum', enum: TYPES_ENUM })
  type: string;

  @Column({ type: 'varchar', length: 512 })
  answers: string;
}
