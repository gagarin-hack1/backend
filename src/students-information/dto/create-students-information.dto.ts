import { TYPES_ENUM } from '../entities/students-information.entity';

export class CreateStudentsInformationDto {
  studentId: number;
  type: TYPES_ENUM;
  answers: string;
}
