import {
    BadRequestException,
    ConflictException,
    Injectable,
    NotFoundException,
    UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { compareSync, genSaltSync, hashSync } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { StudentsService } from '../students/students.service';

@Injectable()
export class AuthService {
    constructor(
      private studentsService: StudentsService,
      private configService: ConfigService,
      private jwtService: JwtService
    ) {}

    async signUp(login: string, pass: string): Promise<{ access_token: string }> {
        const user = await this.studentsService.findOne({ login });
        if (user) throw new ConflictException({ code: 1000, message: 'User already exists' });

        const salt = genSaltSync(5);
        const bcryptPass = hashSync(pass, salt);

        const createdUser = await this.studentsService.create({ login, password: bcryptPass });
        if (!createdUser) throw new BadRequestException();

        const payload = { id: createdUser.id, login: createdUser.login };
        return {
            access_token: await this.jwtService.signAsync(payload),
        };
    }

    async signIn(login: string, pass: string): Promise<{ access_token: string }> {
        const user = await this.studentsService.findOne({ login });
        if (!user) throw new NotFoundException();

        const passSing = compareSync(pass, user.password);
        if (!passSing) throw new UnauthorizedException();

        const payload = { id: user.id, login: user.login };
        return {
            access_token: await this.jwtService.signAsync(payload),
        };
    }
}
