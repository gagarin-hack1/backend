import { Injectable } from '@nestjs/common';
import { CreateStudentsInformationDto } from './dto/create-students-information.dto';
import { UpdateStudentsInformationDto } from './dto/update-students-information.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { StudentInformation } from './entities/students-information.entity';

@Injectable()
export class StudentsInformationService {
  @InjectRepository(StudentInformation) studentInformationEntityRepository: Repository<StudentInformation>;

  async create(createStudentsInformationDto: CreateStudentsInformationDto): Promise<StudentInformation> {
    const userInformation = this.studentInformationEntityRepository.create(createStudentsInformationDto);
    await this.studentInformationEntityRepository.save(userInformation);
    return userInformation;
  }

  findAll(payload) {
    return this.studentInformationEntityRepository.find({
      where: {
        ...payload
      }
    });
  }

  findOne(payload:{ id?: number, login?: string }) {
    return this.studentInformationEntityRepository.findOne({
      where: {
        ...payload
      },
    });
  }

  update(id: number, updateStudentsInformationDto: UpdateStudentsInformationDto) {
    return this.studentInformationEntityRepository.update(id, updateStudentsInformationDto);
  }

  remove(id: number) {
    return this.studentInformationEntityRepository.delete(id);
  }
}
