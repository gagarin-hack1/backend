import { Injectable } from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Student } from './entities/student.entity';

@Injectable()
export class StudentsService {
  @InjectRepository(Student) studentEntityRepository: Repository<Student>;

  async create(createStudentDto: CreateStudentDto): Promise<Student> {
    const user = this.studentEntityRepository.create(createStudentDto);
    await this.studentEntityRepository.save(user);
    return user;
  }

  findAll() {
    return this.studentEntityRepository.find();
  }

  findOne(payload:{ id?: number, login?: string }) {
    return this.studentEntityRepository.findOne({
      where: {
        ...payload
      },
    });
  }

  update(id: number, updateStudentDto: UpdateStudentDto) {
    return this.studentEntityRepository.update(id, updateStudentDto);
  }

  remove(id: number) {
    return this.studentEntityRepository.delete(id);
  }
}
