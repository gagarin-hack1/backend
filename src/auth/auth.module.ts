import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import JwtConfig from '../config/jwt.service';
import { ConfigModule } from '@nestjs/config';
import { AuthGuard } from './auth.guard';
import { APP_GUARD } from '@nestjs/core';
import { StudentsModule } from '../students/students.module';

@Module({
  imports: [
    ConfigModule,
    StudentsModule,
    JwtModule.registerAsync(JwtConfig)
  ],
  providers: [
      AuthService,
      {
          provide: APP_GUARD,
          useClass: AuthGuard,
      },
  ],
  controllers: [AuthController]
})
export class AuthModule {}
