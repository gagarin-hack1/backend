import { Test, TestingModule } from '@nestjs/testing';
import { StudentsInformationController } from './students-information.controller';
import { StudentsInformationService } from './students-information.service';

describe('StudentsInformationController', () => {
  let controller: StudentsInformationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentsInformationController],
      providers: [StudentsInformationService],
    }).compile();

    controller = module.get<StudentsInformationController>(StudentsInformationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
