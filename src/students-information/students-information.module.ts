import { Module } from '@nestjs/common';
import { StudentsInformationService } from './students-information.service';
import { StudentsInformationController } from './students-information.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentInformation } from './entities/students-information.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StudentInformation
    ])
  ],
  controllers: [StudentsInformationController],
  providers: [StudentsInformationService],
})
export class StudentsInformationModule {}
