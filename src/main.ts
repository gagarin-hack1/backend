import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as process from 'process';
import { join } from 'path';
import {NestExpressApplication} from "@nestjs/platform-express";

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(
      AppModule,
  );

  app.enableCors();
  app.setGlobalPrefix('/v1/');
  app.useStaticAssets(join(__dirname, '../public'), {
    prefix: '/public/'
  });

  await app.listen(7111, process.env.NODE_ENV === 'production' ? 'gagarin-api.foowe.ru' : null);
}
bootstrap();
