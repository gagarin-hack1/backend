import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum IClub {
  'design' = 'design',
  'hackathon' = 'hackathon',
  'ctf' = 'ctf',
  'gamedev' = 'gamedev',
  'none' = 'none',
}

@Entity('students')
export class Student {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 16 })
  login: string;

  @Column({ type: 'varchar', length: 255 })
  password: string;

  @Column({ type: 'varchar', length: 16, nullable: true })
  name: string;

  @Column({ type: 'varchar', length: 16, nullable: true })
  surname: string;

  @Column({ type: 'tinyint', nullable: true })
  sex: number;

  @Column({ type: 'varchar', length: 255, nullable: true })
  domain: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  hobby: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  sport: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  programming_language: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  avatar: string;

  @Column({ type: 'enum', enum: IClub, default: IClub.none, nullable: true })
  club: string;

  @Column({ type: 'blob', default: '[]', nullable: true })
  friends: string;
}
