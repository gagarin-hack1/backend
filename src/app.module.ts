import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import databaseService from './config/database.service';
import { AuthModule } from './auth/auth.module';
import { StudentsModule } from './students/students.module';
import { StudentsInformationModule } from './students-information/students-information.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync(databaseService),
    AuthModule,
    StudentsModule,
    StudentsInformationModule
  ],
  providers: [ConfigService]

})
export class AppModule { }
