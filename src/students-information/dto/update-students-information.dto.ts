import { PartialType } from '@nestjs/mapped-types';
import { CreateStudentsInformationDto } from './create-students-information.dto';

export class UpdateStudentsInformationDto extends PartialType(CreateStudentsInformationDto) {}
