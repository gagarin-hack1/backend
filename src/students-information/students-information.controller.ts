import { Controller, Get, Post, Body, Request, NotFoundException } from '@nestjs/common';
import { StudentsInformationService } from './students-information.service';
import { CreateStudentsInformationDto } from './dto/create-students-information.dto';
import { StudentInformation } from './entities/students-information.entity';
import axios, { AxiosResponse } from 'axios';

@Controller('students-information')
export class StudentsInformationController {
  constructor(
    private readonly studentsInformationService: StudentsInformationService,
  ) {}

  private async sendDataToApi(endpoint: string, data: any): Promise<any> {
    try {
      const response: AxiosResponse = await axios.post(`http://185.251.89.123:8080/${endpoint}`, data);
      return response.data;
    } catch (error) {
      // Обработка ошибки
      console.error(error);
      throw new Error(`Failed to send data: ${error.message}`);
    }
  }

  private sortResults(data) {
    // Создаем массив из результатов с ключами и значениями
    const items = Object.keys(data.result).map(key => ({
      course: data.Course[key],
      result: data.result[key],
      index: key
    }));

    // Сортируем массив по значению result в убывающем порядке
    items.sort((a, b) => b.result - a.result);

    // Преобразовываем отсортированный массив обратно в нужную структуру
    const sortedResults = {
      Course: {},
      result: {}
    };

    items.forEach((item, index) => {
      sortedResults.Course[index] = item.course;
      sortedResults.result[index] = item.result;
    });

    return sortedResults;
  }


  @Post()
  create(@Request() req, @Body() createStudentsInformationDto: CreateStudentsInformationDto) {
    const { id } = req.user;
    return this.studentsInformationService.create({ studentId: id, ...createStudentsInformationDto });
  }

  @Get('getCourses')
  async getCourses(@Request() req) {
    if (!req.user) throw new NotFoundException('User does not exist');
    const { id } = req.user;

    const studentInformation: StudentInformation[] = await this.studentsInformationService.findAll({
      studentId: +id
    });

    let answersArray = [];

    for (const studentInformationKey of studentInformation) {
      answersArray = answersArray.concat(studentInformationKey.answers.split(','));
    }

    let resultML = await this.sendDataToApi('getCourses', { description: answersArray.join(',') });
    const sortedResultML = this.sortResults(resultML);

    const result = [];

    for (let i = 0; i < 5; i++) {
      result.push(sortedResultML.Course[i]);
    }

    return result;
  }

  @Get('getFriends')
  async getFriends(@Request() req) {
    if (!req.user) throw new NotFoundException('User does not exist');
    const { id } = req.user;

    const studentInformation: StudentInformation[] = await this.studentsInformationService.findAll({
      studentId: +id
    });

    let answersArray = [];

    for (const studentInformationKey of studentInformation) {
      answersArray = answersArray.concat(studentInformationKey.answers.split(','));
    }

    let resultML = await this.sendDataToApi('getFriends',{ description: answersArray.join(',') });

    return Object.values(resultML);
  }
}
